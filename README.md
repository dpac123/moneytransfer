
## Money Transfer Java REST API

A standalone java program which exposes set of REST APIs for creating accounts, transfer funds between them and recording these transactions.

This program can be run via **com.deepak.moneytransfer.Main** which spins up an HttpServer, initializes test data using in memory **H2 DB** and exposes the APIs on the localhost port 8081.

All tests can be run via **com.deepak.moneytransfer.AccountResourceTest** or by running **mvn clean install** command.  

** Base URL ** : http://localhost:8081/moneytransfer/account

---

## URIs:

1. **GET - /list**	[Retrieve list of all created accounts]
2. **GET - /list/accountId**	[Retrieve an account by accountId]
3. **POST - /create**		[Create an account]
4. **POST - /transfer**		[Transfer funds between accounts]
5. **GET - /transaction/list**		[Retrieve list of all transfer transactions]	
6. **GET - /transaction/list/transactionId**		[Retrieve a transfer transaction by transactionId]

## Sample POST requests:

1. **/create** account
	
	{
		"userName": "dpac",
		"balance": 400.000,
		"currencyCode": "EUR"
	}
	
	
2. **/transfer** funds
	
	{
		"sourceAccountId": 10001,
		"destinationAccountId":10002,
		"amount": 150,
		"currencyCode": "INR",
		"comments": "CC"
	}
