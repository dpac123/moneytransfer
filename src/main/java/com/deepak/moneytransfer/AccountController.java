package com.deepak.moneytransfer;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.deepak.moneytransfer.exception.MTException;
import com.deepak.moneytransfer.model.Account;
import com.deepak.moneytransfer.model.Transaction;
import com.deepak.moneytransfer.svc.AccountService;
import com.deepak.moneytransfer.svc.impl.AccountServiceImpl;

/**
 * Account related resources
 */
@Path("account")
public class AccountController {

	private static final AccountService accountSvc = AccountServiceImpl.getInstance();
	
	@GET
	@Path("list")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Account> getAccounts() throws MTException {
		return accountSvc.getAllAccounts();
	}
	
	@GET
    @Path("list/{accountId}")
    public Account getAccount(@PathParam("accountId") long accountId) throws MTException {
        return accountSvc.getAccountById(accountId);
    }
	
	@POST
    @Path("/create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public Account createAccount(Account account) throws Exception {
        final long accountId = accountSvc.createAccount(account);
        return accountSvc.getAccountById(accountId);
    }
	
	@POST
    @Path("/transfer")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public Account transfer(Transaction transaction) throws Exception {
		return accountSvc.transfer(transaction);
    }
	
	@GET
	@Path("/transaction/list")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Transaction> getTransactions() throws MTException {
		return accountSvc.getAllTransactions();
	}
	
	@GET
    @Path("transaction/list/{transactionId}")
    public Transaction getTransaction(@PathParam("transactionId") long transactionId) throws MTException {
        return accountSvc.getTransactionById(transactionId);
    }
    
}
