package com.deepak.moneytransfer.dao;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;
import com.deepak.moneytransfer.exception.MTException;
import com.deepak.moneytransfer.model.Account;
import com.deepak.moneytransfer.model.Transaction;

/**
 * DAO class for Account related operations
 * 
 * @author Deepak
 */
public interface AccountDAO {
	
	/**
	 * Retrieve all the account details
	 * 
	 * @return List<Account>
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	List<Account> getAllAccounts() throws ClassNotFoundException, SQLException;
	
	/**
     * Retrieve account info for a given accountId
     * 
     * @param accountId
     * @return Account
     * @throws MTException
     */
    Account getAccountById(long accountId) throws SQLException, ClassNotFoundException;
    
    /**
     * Create a new account for the given details
     * 
     * @param account
     * @return long accountId
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws MTException
     */
    long createAccount(Account account) throws SQLException, ClassNotFoundException, MTException;
    
    /**
     * Update the account balance, lock the H2 DB row for this account while updating.
     * 
     * @param accountId
     * @param deltaAmount
     * @return int updatedRows
     * @throws ClassNotFoundException
     * @throws MTException
     * @throws SQLException
     */
    int updateAccountBalance(long accountId, BigDecimal deltaAmount) throws ClassNotFoundException, MTException, SQLException;
    
    /**
     * Retrieve all the transactions
     * 
     * @return List<Transaction>
     * @throws MTException
     */
    List<Transaction> getAllTransactions() throws SQLException, ClassNotFoundException;
    
    /**
     * Retrieve a transaction info for given transactionId
     * 
     * @param transactionId
     * @return Transaction
     * @throws MTException
     */
    Transaction getTransactionById(long transactionId) throws SQLException, ClassNotFoundException;

    /**
     * Create or record the transfer transaction
     * 
     * @param transaction
     * @return long transactionId
     * @throws ClassNotFoundException
     * @throws MTException
     * @throws SQLException
     */
	long createTransaction(Transaction transaction) throws ClassNotFoundException, MTException, SQLException;
}
