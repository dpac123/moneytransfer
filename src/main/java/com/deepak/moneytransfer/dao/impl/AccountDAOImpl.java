package com.deepak.moneytransfer.dao.impl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.deepak.moneytransfer.dao.AccountDAO;
import com.deepak.moneytransfer.exception.ErrorConstants;
import com.deepak.moneytransfer.exception.MTException;
import com.deepak.moneytransfer.model.Account;
import com.deepak.moneytransfer.model.Transaction;
import com.deepak.moneytransfer.util.H2DBUtil;

/**
 * H2 DB DAO Impl class for Account related operations
 * 
 * @author Deepak
 *
 */
public class AccountDAOImpl implements AccountDAO {
	
	private final static String SQL_GET_ACC_BY_ID = "SELECT * FROM Account WHERE id = ? ";
	private final static String SQL_LOCK_ACC_BY_ID = "SELECT * FROM Account WHERE id = ? FOR UPDATE";
	private final static String SQL_CREATE_ACC = "INSERT INTO Account (id, UserName, Balance, CurrencyCode, Status) VALUES (?, ?, ?, ?, ?)";
	private final static String SQL_UPDATE_ACC_BALANCE = "UPDATE Account SET Balance = ? WHERE id = ? ";
	private final static String SQL_GET_ALL_ACC = "SELECT * FROM Account";
	private final static String SQL_GET_ALL_TXN = "SELECT * FROM Transaction";
	private final static String SQL_GET_TXN_BY_ID = "SELECT * FROM Transaction WHERE id = ? ";
	private final static String SQL_CREATE_TXN = "INSERT INTO Transaction (id, SourceAccountId, DestinationAccountId, Amount, CurrencyCode, Status, Comments) "
			+ "VALUES (?, ?, ?, ?, ?, ?, ?)";
	
	// Account table/column constants
	private static final String ACC_ID = "id";
	private static final String ACC_USERNAME = "UserName";
	private static final String ACC_BALANCE = "Balance";
	private static final String ACC_CURRENCY = "CurrencyCode";
	private static final String ACC_STATUS = "Status";
	
	// Transaction table/column constants
	private static final String TXN_ID = "id";
	private static final String TXN_SRC_ACC_ID = "SourceAccountId";
	private static final String TXN_DEST_ACC_ID = "DestinationAccountId";
	private static final String TXN_AMOUNT = "Amount";
	private static final String TXN_CURRENCY = "CurrencyCode";
	private static final String TXN_STATUS = "Status";
	private static final String TXN_COMMENTS = "Comments";
	
	
	private static AccountDAOImpl impl = null;
	
	private AccountDAOImpl() {}
	
	// get a singleton instance of this class
	public static AccountDAOImpl getInstance( ) {
		if (impl == null) {
			synchronized(AccountDAOImpl.class) {
				if (impl == null) {
					impl = new AccountDAOImpl();
				}
			}
		}
		return impl;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Account> getAllAccounts() throws ClassNotFoundException, SQLException {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Account> allAccounts = new ArrayList<Account>();
		try {
			conn = H2DBUtil.getConnection();
			stmt = conn.prepareStatement(SQL_GET_ALL_ACC);
			rs = stmt.executeQuery();
			while (rs.next()) {
				Account acc = new Account(rs.getLong(ACC_ID), rs.getString(ACC_USERNAME),
						rs.getBigDecimal(ACC_BALANCE), rs.getString(ACC_CURRENCY), rs.getString(ACC_STATUS));
				
				System.out.println("getAllAccounts(): Get  Account " + acc);
				allAccounts.add(acc);
			}
			return allAccounts;
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Account getAccountById(long accountId) throws SQLException, ClassNotFoundException {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Account acc = null;
		try {
			conn = H2DBUtil.getConnection();
			stmt = conn.prepareStatement(SQL_GET_ACC_BY_ID);
			stmt.setLong(1, accountId);
			rs = stmt.executeQuery();
			if (rs.next()) {
				acc = new Account(rs.getLong(ACC_ID), rs.getString(ACC_USERNAME),
						rs.getBigDecimal(ACC_BALANCE), rs.getString(ACC_CURRENCY), rs.getString(ACC_STATUS));
				
				System.out.println("Retrieve Account By Id: " + acc);
			}
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		return acc;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long createAccount(Account account) throws SQLException, ClassNotFoundException, MTException {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = H2DBUtil.getConnection();
			stmt = conn.prepareStatement(SQL_CREATE_ACC);
			stmt.setLong(1, account.getId());
			stmt.setString(2, account.getUserName());
			stmt.setBigDecimal(3, account.getBalance());
			stmt.setString(4, account.getCurrencyCode());
			stmt.setString(5, String.valueOf(account.getStatus()));
			int affectedRows = stmt.executeUpdate();
			System.out.println("inserting account: " + account + " res: " + affectedRows);
			if (affectedRows == 0) {
				System.out.println("createAccount(): Creating account failed, no rows affected.");
				throw new MTException(ErrorConstants.DAO_CREATE_ACC_FAIL);
			}
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		return account.getId();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int updateAccountBalance(long accountId, BigDecimal deltaAmount) throws ClassNotFoundException, MTException, SQLException  {
		Connection conn = null;
		PreparedStatement lockStmt = null;
		PreparedStatement updateStmt = null;
		ResultSet rs = null;
		Account targetAccount = null;
		int updateCount = -1;
		try {
			conn = H2DBUtil.getConnection();
			conn.setAutoCommit(false);
			// lock account for updating the balance
			lockStmt = conn.prepareStatement(SQL_LOCK_ACC_BY_ID);
			lockStmt.setLong(1, accountId);
			rs = lockStmt.executeQuery();
			if (rs.next()) {
				targetAccount = new Account(rs.getLong(ACC_ID), rs.getString(ACC_USERNAME),
						rs.getBigDecimal(ACC_BALANCE), rs.getString(ACC_CURRENCY), rs.getString(ACC_STATUS));
				
				System.out.println("updateAccountBalance for Account: " + targetAccount);
			}

			if (targetAccount == null) {
				throw new MTException(ErrorConstants.DAO_ACC_LOCK_FAIL + accountId);
			}

			updateStmt = conn.prepareStatement(SQL_UPDATE_ACC_BALANCE);
			updateStmt.setBigDecimal(1, deltaAmount);
			updateStmt.setLong(2, accountId);
			updateCount = updateStmt.executeUpdate();
			conn.commit();
			
			return updateCount;
		} catch (SQLException se) {
			// rollback transaction if exception occurs
			System.out.println("updateAccountBalance(): Transaction Failed, rollback initiated for: " + accountId + se);
			try {
				if (conn != null)
					conn.rollback();
			} catch (SQLException re) {
				throw new MTException(ErrorConstants.DAO_TXN_ROLLBACK_FAIL + re);
			}
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		return updateCount;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long createTransaction(Transaction transaction) throws ClassNotFoundException, MTException, SQLException  {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = H2DBUtil.getConnection();
			stmt = conn.prepareStatement(SQL_CREATE_TXN);
			stmt.setLong(1, transaction.getId());
			stmt.setLong(2, transaction.getSourceAccountId());
			stmt.setLong(3, transaction.getDestinationAccountId());
			stmt.setBigDecimal(4, transaction.getAmount());
			stmt.setString(5, transaction.getCurrencyCode());
			stmt.setString(6, String.valueOf(transaction.getStatus()));
			stmt.setString(7, transaction.getComments());
			int affectedRows = stmt.executeUpdate();
			System.out.println("inserting transaction: "+transaction+" res: "+affectedRows);
			if (affectedRows == 0) {
				System.out.println("createTransaction(): Creating transaction failed, no rows affected.");
				throw new MTException(ErrorConstants.DAO_CREATE_TXN_FAIL);
			}
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		return transaction.getId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Transaction> getAllTransactions() throws SQLException, ClassNotFoundException {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Transaction> txns = new ArrayList<Transaction>();
		try {
			conn = H2DBUtil.getConnection();
			stmt = conn.prepareStatement(SQL_GET_ALL_TXN);
			rs = stmt.executeQuery();
			while (rs.next()) {
				Transaction txn = new Transaction(rs.getLong(TXN_ID), rs.getLong(TXN_SRC_ACC_ID), rs.getLong(TXN_DEST_ACC_ID), 
						rs.getBigDecimal(TXN_AMOUNT), rs.getString(TXN_CURRENCY), rs.getString(TXN_COMMENTS), 
						rs.getString(TXN_STATUS));
				
				System.out.println("getAllTransactions(): Get  Transaction " + txn);
				txns.add(txn);
			}
			return txns;
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Transaction getTransactionById(long transactionId) throws SQLException, ClassNotFoundException {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Transaction txn = null;
		try {
			conn = H2DBUtil.getConnection();
			stmt = conn.prepareStatement(SQL_GET_TXN_BY_ID);
			stmt.setLong(1, transactionId);
			rs = stmt.executeQuery();
			if (rs.next()) {
				txn = new Transaction(rs.getLong(TXN_ID), rs.getLong(TXN_SRC_ACC_ID), rs.getLong(TXN_DEST_ACC_ID), 
						rs.getBigDecimal(TXN_AMOUNT), rs.getString(TXN_CURRENCY), rs.getString(TXN_COMMENTS), 
						rs.getString(TXN_STATUS));
				
				System.out.println("getTransactionById(): Retrieve Transaction " + txn);
			}
			return txn;
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}

}
