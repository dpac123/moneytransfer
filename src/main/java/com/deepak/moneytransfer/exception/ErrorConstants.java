package com.deepak.moneytransfer.exception;

public interface ErrorConstants {

	// service layer errors
	public static final String BAD_ACCOUNT_DETAILS = "Bad request containing account details";
	public static final String INVALID_ACCOUNT_ID = "Invalid account id";
	public static final String INVALID_TXN_ID = "Invalid transaction id";
	public static final String LOW_ACCT_BALANCE = "Low Account balance";
	public static final String CURRENCY_MISMATCH = "Currency mismatch";
	
	
	// dao layer errors
	public static final String DAO_CREATE_ACC_FAIL = "Account could not be created";
	public static final String DAO_CREATE_TXN_FAIL = "Transaction could not be created";
	public static final String DAO_ACC_LOCK_FAIL = "Failed to lock account";
	public static final String DAO_TXN_ROLLBACK_FAIL = "Transaction Rollback failed";
	
}
