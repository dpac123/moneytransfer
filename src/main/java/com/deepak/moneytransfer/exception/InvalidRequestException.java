package com.deepak.moneytransfer.exception;

public class InvalidRequestException extends MTException {

	public InvalidRequestException(String message) {
		super(message);
	}

	private static final long serialVersionUID = 1L;

}
