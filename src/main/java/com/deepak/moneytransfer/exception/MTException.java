package com.deepak.moneytransfer.exception;

public class MTException extends Exception {

	private static final long serialVersionUID = 1L;

	public MTException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MTException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public MTException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public MTException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public MTException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
}
