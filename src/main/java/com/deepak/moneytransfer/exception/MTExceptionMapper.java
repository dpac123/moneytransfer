package com.deepak.moneytransfer.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class MTExceptionMapper implements ExceptionMapper<MTException>{

	public MTExceptionMapper() {}
	
	@Override
	public Response toResponse(MTException exception) {
		Response.Status httpStatus = Response.Status.INTERNAL_SERVER_ERROR;
		if (exception instanceof InvalidRequestException) {
			httpStatus = Response.Status.BAD_REQUEST;
		}
		
		MTResponse errorResponse = new MTResponse();
		errorResponse.setErrorCode(exception.getMessage());
		
		return Response.status(httpStatus).entity(errorResponse).build();
	}

}
