package com.deepak.moneytransfer.exception;

public class MTResponse {
	private String errorCode;

    public String getErrorCode() {
        return errorCode;
    }
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
