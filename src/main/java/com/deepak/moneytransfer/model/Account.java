package com.deepak.moneytransfer.model;

import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Account business class
 * 
 * @author Deepak
 */
public class Account {
	
	private static final AtomicLong COUNTER = new AtomicLong(10001); // using atomic for thread safety while assigning account id
	
    private final long id;
	
	private String userName; // using username for readability instead of userId
	
    private BigDecimal balance;
	
    private String currencyCode;
    
    private Status status = Status.ACTIVE;

    /**
     * This constructor would be used by controller to marshal incoming json request 
     * to Account POJO. Hence no need to set valid account id.
     */
    public Account() {
    	this.id = 0L; // initialize to 0 as this would not be used to persist in DB
    };
    
    /**
     * This constructor would be used to create a new account in H2 DB.
     * Hence accountId should be incremented accordingly.
     * 
     * @param userName
     * @param balance
     * @param currencyCode
     */
	public Account(String userName, BigDecimal balance, String currencyCode) {
		super();
		this.id = COUNTER.getAndIncrement();
		this.userName = userName;
		this.balance = balance;
		this.currencyCode = currencyCode;
	}
	
	/**
	 * This constructor would be used by DAO layer to construct POJO from H2 DB account data
	 * 
	 * @param id
	 * @param userName
	 * @param balance
	 * @param currencyCode
	 * @param status
	 */
	public Account(long id, String userName, BigDecimal balance, String currencyCode, String status) {
		super();
		this.id = id;
		this.userName = userName;
		this.balance = balance;
		this.currencyCode = currencyCode;
		this.status = Status.valueOf(status);
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public long getId() {
		return id;
	}
	
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
	public void withdraw(BigDecimal amount) {
        this.balance = balance.subtract(amount);
    }

    public void deposit(BigDecimal amount) {
        this.balance = balance.add(amount);
    }

	@Override
	public String toString() {
		return "Account [id=" + id + ", userName=" + userName + ", balance=" + balance + ", currencyCode="
				+ currencyCode + "]";
	}

	public enum Status {
		ACTIVE,
		DEACTIVE
	}
}
