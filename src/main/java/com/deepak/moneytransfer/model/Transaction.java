package com.deepak.moneytransfer.model;

import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Transaction business class
 * 
 * @author Deepak
 */
public class Transaction {
	
	private static final AtomicLong COUNTER = new AtomicLong(50001);

    private final long id;

    private long sourceAccountId;

    private long destinationAccountId;

    private BigDecimal amount;

    private String currencyCode;

    private String comments;

    private Status status = Status.PENDING;

    /**
     * This constructor would be used by controller to marshal incoming json request 
     * to Transaction POJO. Hence no need to set valid transaction id.
     */
    public Transaction() {
    	this.id = 0L;
    }
    
    /**
     * This constructor would be used to create a new transaction in H2 DB.
     * Hence transactionId should be incremented accordingly.
     * 
     * @param sourceAccountId
     * @param destinationAccountId
     * @param amount
     * @param currencyCode
     * @param comments
     * @param status
     */
    public Transaction(long sourceAccountId, long destinationAccountId, BigDecimal amount, String currencyCode,
			String comments, Status status) {
		super();
		this.id = COUNTER.getAndIncrement();
		this.sourceAccountId = sourceAccountId;
		this.destinationAccountId = destinationAccountId;
		this.amount = amount;
		this.currencyCode = currencyCode;
		this.comments = comments;
		this.status = status;
	}
    
    /**
     * This constructor would be used by DAO layer to construct POJO from H2 DB transaction data.
     * 
     * @param id
     * @param sourceAccountId
     * @param destinationAccountId
     * @param amount
     * @param currencyCode
     * @param comments
     * @param status
     */
	public Transaction(long id, long sourceAccountId, long destinationAccountId, BigDecimal amount, String currencyCode,
			String comments, String status) {
		super();
		this.id = id;
		this.sourceAccountId = sourceAccountId;
		this.destinationAccountId = destinationAccountId;
		this.amount = amount;
		this.currencyCode = currencyCode;
		this.comments = comments;
		this.status = Status.valueOf(status);
	}
 
	@Override
	public String toString() {
		return "Transaction [id=" + id + ", sourceAccountId=" + sourceAccountId + ", destinationAccountId="
				+ destinationAccountId + ", amount=" + amount + ", currencyCode=" + currencyCode + ", comments="
				+ comments + ", status=" + status + "]";
	}


	public long getSourceAccountId() {
		return sourceAccountId;
	}



	public void setSourceAccountId(long sourceAccountId) {
		this.sourceAccountId = sourceAccountId;
	}



	public long getDestinationAccountId() {
		return destinationAccountId;
	}



	public void setDestinationAccountId(long destinationAccountId) {
		this.destinationAccountId = destinationAccountId;
	}



	public BigDecimal getAmount() {
		return amount;
	}



	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}



	public String getCurrencyCode() {
		return currencyCode;
	}



	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}



	public String getComments() {
		return comments;
	}



	public void setComments(String comments) {
		this.comments = comments;
	}



	public Status getStatus() {
		return status;
	}



	public void setStatus(Status status) {
		this.status = status;
	}



	public long getId() {
		return id;
	}

	
	public enum Status {
		PENDING,
		COMPLETED,
		FAILED
	}
    
}
