package com.deepak.moneytransfer.svc;

import java.util.List;
import com.deepak.moneytransfer.exception.MTException;
import com.deepak.moneytransfer.model.Account;
import com.deepak.moneytransfer.model.Transaction;

/**
 * Service class for Account
 * 
 * @author Deepak
 */
public interface AccountService {
	
	/**
	 * Retrieve all the account details
	 * 
	 * @return List<Account>
	 * @throws MTException
	 */
    public List<Account> getAllAccounts() throws MTException;
    
    /**
     * Retrieve account info for a given accountId
     * 
     * @param accountId
     * @return Account
     * @throws MTException
     */
    public Account getAccountById(long accountId) throws MTException;
    
    /**
     * Create a new account for the given details
     * 
     * @param account
     * @return long accountId
     * @throws Exception
     */
    long createAccount(Account account) throws MTException;
    
    /**
     * Transfer amount between the given accounts. This is a critical
     * operation which needs to be thread safe.
     * 
     * @param transaction
     * @return Account updatedDestinationAccount
     * @throws MTException
     */
    Account transfer(Transaction transaction) throws MTException;
    
    /**
     * Retrieve all the transactions
     * 
     * @return List<Transaction>
     * @throws MTException
     */
    public List<Transaction> getAllTransactions() throws MTException;
    
    /**
     * Retrieve a transaction info for given transactionId
     * 
     * @param transactionId
     * @return Transaction
     * @throws MTException
     */
    public Transaction getTransactionById(long transactionId) throws MTException;
}
