package com.deepak.moneytransfer.svc.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import com.deepak.moneytransfer.dao.AccountDAO;
import com.deepak.moneytransfer.dao.impl.AccountDAOImpl;
import com.deepak.moneytransfer.exception.ErrorConstants;
import com.deepak.moneytransfer.exception.InvalidRequestException;
import com.deepak.moneytransfer.exception.MTException;
import com.deepak.moneytransfer.model.Account;
import com.deepak.moneytransfer.model.Transaction;
import com.deepak.moneytransfer.svc.AccountService;

public class AccountServiceImpl implements AccountService {

	private static AccountServiceImpl svcImpl = null;

	private final AccountDAO dao = AccountDAOImpl.getInstance();
	
	private AccountServiceImpl() {}
	
	// get a singleton instance of this class
	public static AccountServiceImpl getInstance( ) {
		if (svcImpl == null) {
			synchronized(AccountServiceImpl.class) {
				if (svcImpl == null) {
					svcImpl = new AccountServiceImpl();
				}
			}
		}
		return svcImpl;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public long createAccount(Account account) throws MTException{
		long accountId = 0;
		try {
			if (account.getUserName() == null || account.getUserName().isEmpty() ||
				account.getCurrencyCode() == null || account.getCurrencyCode().isEmpty() || account.getCurrencyCode().length() != 3) {
				throw new InvalidRequestException(ErrorConstants.BAD_ACCOUNT_DETAILS);
			}
			accountId = dao.createAccount(new Account(account.getUserName(), account.getBalance(), account.getCurrencyCode()));
		} catch (Exception e) {
			System.out.println("createAccount(): " + e);
			throw new MTException(e.getMessage());
		}
		return accountId;
	}
	
	/**
	 * {@inheritDoc}
	 */
    public List<Account> getAllAccounts() throws MTException{
    	List<Account> accounts = new ArrayList<Account>();
        try {
			accounts = dao.getAllAccounts();
		} catch (Exception e) {
			System.out.println("getAllAccounts(): " + e);
			throw new MTException(e.getMessage());
		}
        
        return accounts;
    }
    
    /**
	 * {@inheritDoc}
	 */
    public Account getAccountById(long accountId) throws MTException{
    	Account acct = null;
    	try {
    		if (accountId <= 0) {
    			throw new InvalidRequestException(ErrorConstants.INVALID_ACCOUNT_ID);
    		}
			acct = dao.getAccountById(accountId);
			if (acct == null) {
				throw new InvalidRequestException(ErrorConstants.INVALID_ACCOUNT_ID);
			}
		} catch (Exception e) {
			System.out.println("getAccountById(): " + e);
			throw new MTException(e.getMessage());
		}
    	return acct;
    }

    /**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized Account transfer(Transaction transaction) throws MTException {
		Account updatedAcct = null;
		try {
			Account fromAcct = dao.getAccountById(transaction.getSourceAccountId());
			Account toAcct = dao.getAccountById(transaction.getDestinationAccountId());
			BigDecimal amount = transaction.getAmount();
			
			if (fromAcct == null || toAcct == null) {
				throw new InvalidRequestException(ErrorConstants.INVALID_ACCOUNT_ID);
			}
			// if amount is greater than balance of from account, throw exception
			if (amount.compareTo(fromAcct.getBalance()) == 1) {
				throw new InvalidRequestException(ErrorConstants.LOW_ACCT_BALANCE);
			}
			// validate currencyCode
			if (!fromAcct.getCurrencyCode().equals(toAcct.getCurrencyCode()) || 
					!toAcct.getCurrencyCode().equals(transaction.getCurrencyCode())) {
				throw new InvalidRequestException(ErrorConstants.CURRENCY_MISMATCH);
			}
			
			fromAcct.withdraw(amount);
			toAcct.deposit(amount);
			
			dao.updateAccountBalance(fromAcct.getId(), fromAcct.getBalance());
			dao.updateAccountBalance(toAcct.getId(), toAcct.getBalance());
			dao.createTransaction(new Transaction(transaction.getSourceAccountId(), transaction.getDestinationAccountId(),
					amount, transaction.getCurrencyCode(), transaction.getComments(), Transaction.Status.COMPLETED));
			
			updatedAcct = dao.getAccountById(toAcct.getId());
		} catch (Exception e) {
			System.out.println("transfer(): " + e);
			throw new MTException(e.getMessage());
		}
		
		return updatedAcct;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Transaction> getAllTransactions() throws MTException {
		List<Transaction> txns = new ArrayList<Transaction>();
        try {
        	txns = dao.getAllTransactions();
		} catch (Exception e) {
			System.out.println("getAllTransactions(): " + e);
			throw new MTException(e.getMessage());
		}
        
        return txns;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Transaction getTransactionById(long transactionId) throws MTException {
		Transaction txn = null;
    	try {
    		if (transactionId <= 0) {
    			throw new InvalidRequestException(ErrorConstants.INVALID_TXN_ID);
    		}
    		txn = dao.getTransactionById(transactionId);
    		if (txn == null) {
    			throw new InvalidRequestException(ErrorConstants.INVALID_TXN_ID);
    		}
		} catch (Exception e) {
			System.out.println("getTransactionById(): " + e);
			throw new MTException(e.getMessage());
		}
    	return txn;
	}
}
