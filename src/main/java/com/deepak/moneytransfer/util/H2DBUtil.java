package com.deepak.moneytransfer.util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.h2.tools.RunScript;

/**
 * Utility class for creating H2 DB connection and
 * loading test data in H2 DB.
 * 
 * @author Deepak
 *
 */
public class H2DBUtil {
	
	private static final String H2_DRIVER = PropertyLoader.getStringProperty("driverClassName");
	private static final String H2_CONN_URL = PropertyLoader.getStringProperty("url");
	private static final String H2_USER = PropertyLoader.getStringProperty("username");
	private static final String H2_PWD = PropertyLoader.getStringProperty("password");


	public static Connection getConnection() throws SQLException, ClassNotFoundException {
		Class.forName(H2_DRIVER);
		
		return DriverManager.getConnection(H2_CONN_URL, H2_USER, H2_PWD);
	}

	public static void populateTestData() throws SQLException, FileNotFoundException, ClassNotFoundException {
		System.out.println("Populating Test User Table and data ..... ");
		Connection conn = null;
		try {
			conn = H2DBUtil.getConnection();
			RunScript.execute(conn, new FileReader("src/main/resources/testdata.sql"));
		} catch (SQLException e) {
			System.out.println("populateTestData(): Error populating user data: "+e.getMessage());
			throw e;
		} catch (FileNotFoundException e) {
			System.out.println("populateTestData(): Error finding test script file "+e.getMessage());
			throw e;
		} catch (ClassNotFoundException e) {
			System.out.println("populateTestData(): Error class not found "+e.getMessage());
			throw e;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					System.out.println("populateTestData(): Error while closing conn resource");
				}
			}
		}
	}
}
