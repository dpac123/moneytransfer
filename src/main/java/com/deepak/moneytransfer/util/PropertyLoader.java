package com.deepak.moneytransfer.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyLoader {
	private static Properties properties = new Properties();

	static {
        String configFileName = System.getProperty("application.properties");
        if (configFileName == null) {
            configFileName = "application.properties";
        }
        loadConfig(configFileName);
    }
	
    public static void loadConfig(String fileName) {
        if (fileName == null) {
            System.out.println("loadConfig: config file name cannot be null");
        } else {
            try {
            	System.out.println("loadConfig(): Loading config file: " + fileName );
                final InputStream fis = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
                properties.load(fis);
            } catch (FileNotFoundException fne) {
            	System.out.println("loadConfig(): file name not found " + fileName + fne.getMessage());
            } catch (IOException ioe) {
            	System.out.println("loadConfig(): error when reading the config " + fileName + ioe.getMessage());
            }
        }
    }

    public static String getStringProperty(String key) {
        String value = properties.getProperty(key);
        if (value == null) {
            value = System.getProperty(key);
        }
        return value;
    }
}
