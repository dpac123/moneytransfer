DROP TABLE IF EXISTS Account;

CREATE TABLE Account (id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
UserName VARCHAR(30),
Balance DECIMAL(19,4),
CurrencyCode VARCHAR(30),
Status VARCHAR(10)
);

DROP TABLE IF EXISTS Transaction;

CREATE TABLE Transaction (id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
SourceAccountId INTEGER,
DestinationAccountId INTEGER,
Amount DECIMAL(19,4),
CurrencyCode VARCHAR(30),
Status VARCHAR(10),
Comments VARCHAR(60)
);

INSERT INTO Account (UserName,Balance,CurrencyCode,Status) VALUES ('sam',100.0000,'USD','ACTIVE');
INSERT INTO Account (UserName,Balance,CurrencyCode,Status) VALUES ('ram',200.0000,'USD','ACTIVE');
INSERT INTO Account (UserName,Balance,CurrencyCode,Status) VALUES ('dan',500.0000,'EUR','ACTIVE');
INSERT INTO Account (UserName,Balance,CurrencyCode,Status) VALUES ('tom',500.0000,'EUR','ACTIVE');

INSERT INTO Transaction (SourceAccountId,DestinationAccountId,Amount,CurrencyCode,Status,Comments) VALUES (1,2,100.0000,'USD','COMPLETED','Credit transfer');