package com.deepak.moneytransfer;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.grizzly.http.server.HttpServer;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.deepak.moneytransfer.exception.ErrorConstants;
import com.deepak.moneytransfer.exception.MTException;
import com.deepak.moneytransfer.exception.MTResponse;
import com.deepak.moneytransfer.model.Account;
import com.deepak.moneytransfer.model.Transaction;
import com.deepak.moneytransfer.svc.impl.AccountServiceImpl;
import com.deepak.moneytransfer.util.H2DBUtil;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.List;

/**
 * Test class for all the test cases related to Account operations
 * 
 * @author Deepak
 */
@SuppressWarnings("unchecked")
public class AccountResourceTest {

    private static HttpServer server;
    private static WebTarget target;

    @BeforeClass
    public static void setup() throws Exception {
        H2DBUtil.populateTestData();
        
        // start the server
        server = Main.startServer();
        // create the client
        Client c = ClientBuilder.newClient();

        target = c.target(Main.BASE_URI);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        server.stop();
    }
    
    /**
     * Test fund transfer between 2 accounts for multiple threads
     * 
     * During startup, populated test data would have accounts created with Id 3 and 4. Each of these accounts would have a balance of
	 * 500 EUR. 
	 * 1. We first create 50 threads where each thread will transfer 10 EUR from account 3 to 4.
	 * 2. We then create another 25 threads where each thread will transfer 10 EUR from account 4 to 3.
	 * 3. At the end of completion of all these threads, we should have balance of 250 EUR in account 3 and 750 EUR in account 4
     * 
     * @throws InterruptedException
     * @throws MTException
     */
    @Test
    public void testTransferFundsForMultipleThreads() throws InterruptedException, MTException {
    	final AccountServiceImpl svcImpl = AccountServiceImpl.getInstance();
    	
    	// 1. Create 50 threads where each thread transfers 10 EUR from account 3 to 4
    	final Transaction transferFrom3to4 = new Transaction(3, 4, new BigDecimal(10.00), "EUR", "CC", Transaction.Status.COMPLETED);
		for (int i=0; i<50; i++) {
			new Thread(new Runnable() {
				@Override
				public void run() {
						System.out.println("****************** Running transfer of 3 to 4 for " + Thread.currentThread().getName());
						try {
							svcImpl.transfer(transferFrom3to4);
						} catch (MTException e) {
							e.printStackTrace();
						}
				}
			}, "Thread " + i).start();
		}
		
		// 2. Create 25 threads where each thread transfers 10 EUR from account 4 to 3
		final Transaction transferFrom4to3 = new Transaction(4, 3, new BigDecimal(10.00), "EUR", "CC", Transaction.Status.COMPLETED);
		for (int i=0; i<25; i++) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					
					try {
						System.out.println("****************** Running transfer of 4 to 3 for " + Thread.currentThread().getName());
						svcImpl.transfer(transferFrom4to3);
					} catch (MTException e) {
						e.printStackTrace();
					}
				}
				
			}, "Thread " + i).start();
		}
		
		Thread.sleep(3000); // add delay of 3 seconds to ensure all threads are completed before checking final balance.
		
		// 3. Ensure account 3 and 4 has correct balance of 250 and 750 respectively.
		assertEquals(250, svcImpl.getAccountById(3).getBalance().intValue());
		assertEquals(750, svcImpl.getAccountById(4).getBalance().intValue());
    }
    
    /**
     * Positive Scenario:
     * Test retrieval of all created accounts
     * return 200 OK
     */
	@Test
    public void testGetAllAccountsSuccess() {
        Response response = target.path("account/list").request().get();
        ensureOKResponse(response);
        List<Account> content = response.readEntity(List.class);
        assertEquals(5, content.size());
    }
    
	/**
     * Positive Scenario:
     * Test retrieval of a specific account
     * return 200 OK
     */
	@Test
    public void testGetAccountByIdSuccess() {
        Response response = target.path("account/list/4").request().get();
        ensureOKResponse(response);
        Account account = response.readEntity(Account.class);
        assertEquals("tom", account.getUserName());
        assertEquals(500, account.getBalance().intValue());
        assertEquals("ACTIVE", String.valueOf(account.getStatus()));
    }
    
	/**
     * Negative Scenario:
     * Test retrieval of an account which is not present in H2
     * return 500 Internal Server Error
     */
	@Test
    public void testGetAccountByInvalidId() {
        Response response = target.path("account/list/5").request().get();
        ensureFAILResponse(response);
        MTResponse mtresp = response.readEntity(MTResponse.class);
        assertEquals(ErrorConstants.INVALID_ACCOUNT_ID, mtresp.getErrorCode());
    }
	
	/**
     * Positive Scenario:
     * Test creation of a new account
     * return 200 OK
     */
	@Test
    public void testCreateAccountSuccess() {
		Account acct = new Account(0L, "dpac", new BigDecimal(700.00), "INR", "ACTIVE");
        Response response = target.path("account/create").request(MediaType.APPLICATION_JSON_TYPE)
        		.post(Entity.entity(acct, MediaType.APPLICATION_JSON_TYPE));
        ensureOKResponse(response);
        Account createdAccount = response.readEntity(Account.class);
        assertEquals(acct.getUserName(), createdAccount.getUserName());
        assertEquals(700, createdAccount.getBalance().intValue());
        assertEquals(acct.getCurrencyCode(), createdAccount.getCurrencyCode());
    }
	
	/**
     * Negative Scenario:
     * Test creation of a new account for empty username in request
     * return 500 Internal Server Error
     */
	@Test
    public void testCreateAccountForEmptyUserName() {
		Account acct = new Account(0L, "", new BigDecimal(700.00), "INR", "ACTIVE");
        Response response = target.path("account/create").request(MediaType.APPLICATION_JSON_TYPE)
        		.post(Entity.entity(acct, MediaType.APPLICATION_JSON_TYPE));
        ensureFAILResponse(response);
        MTResponse mtresp = response.readEntity(MTResponse.class);
        assertEquals(ErrorConstants.BAD_ACCOUNT_DETAILS, mtresp.getErrorCode());
    }
	
	/**
     * Negative Scenario:
     * Test creation of a new account for empty currency in request
     * return 500 Internal Server Error
     */
	@Test
    public void testCreateAccountForEmptyCurrencyCode() {
		Account acct = new Account(0L, "roy", new BigDecimal(700.00), "", "ACTIVE");
        Response response = target.path("account/create").request(MediaType.APPLICATION_JSON_TYPE)
        		.post(Entity.entity(acct, MediaType.APPLICATION_JSON_TYPE));
        ensureFAILResponse(response);
        MTResponse mtresp = response.readEntity(MTResponse.class);
        assertEquals(ErrorConstants.BAD_ACCOUNT_DETAILS, mtresp.getErrorCode());
    }
	
	/**
     * Negative Scenario:
     * Test creation of a new account for invalid currency in request
     * return 500 Internal Server Error
     */
	@Test
    public void testCreateAccountForInvalidCurrencyCode() {
		Account acct = new Account(0L, "roy", new BigDecimal(700.00), "INSR", "ACTIVE");
        Response response = target.path("account/create").request(MediaType.APPLICATION_JSON_TYPE)
        		.post(Entity.entity(acct, MediaType.APPLICATION_JSON_TYPE));
        ensureFAILResponse(response);
        MTResponse mtresp = response.readEntity(MTResponse.class);
        assertEquals(ErrorConstants.BAD_ACCOUNT_DETAILS, mtresp.getErrorCode());
    }
	
	/**
     * Negative Scenario:
     * Test funds transfer for invalid source account
     * return 500 Internal Server Error
     */
	@Test
    public void testTransferForInvalidSrcAccount() {
		Transaction txn = new Transaction(0L, 21, 3, new BigDecimal(50.00), "EUR", "comments", "PENDING");
        Response response = target.path("account/transfer").request(MediaType.APPLICATION_JSON_TYPE)
        		.post(Entity.entity(txn, MediaType.APPLICATION_JSON_TYPE));
        ensureFAILResponse(response);
        MTResponse mtresp = response.readEntity(MTResponse.class);
        assertEquals(ErrorConstants.INVALID_ACCOUNT_ID, mtresp.getErrorCode());
    }
	
	/**
     * Negative Scenario:
     * Test funds transfer for invalid destination account
     * return 500 Internal Server Error
     */
	@Test
    public void testTransferForInvalidDestAccount() {
		Transaction txn = new Transaction(0L, 2, 31, new BigDecimal(50.00), "EUR", "comments", "PENDING");
        Response response = target.path("account/transfer").request(MediaType.APPLICATION_JSON_TYPE)
        		.post(Entity.entity(txn, MediaType.APPLICATION_JSON_TYPE));
        ensureFAILResponse(response);
        MTResponse mtresp = response.readEntity(MTResponse.class);
        assertEquals(ErrorConstants.INVALID_ACCOUNT_ID, mtresp.getErrorCode());
    }
	
	/**
     * Negative Scenario:
     * Test funds transfer when source and dest account have different currencies
     * return 500 Internal Server Error
     */
	@Test
    public void testTransferForCurrencyMismatchBwAccounts() {
		Transaction txn = new Transaction(0L, 2, 3, new BigDecimal(50.00), "EUR", "comments", "PENDING");
        Response response = target.path("account/transfer").request(MediaType.APPLICATION_JSON_TYPE)
        		.post(Entity.entity(txn, MediaType.APPLICATION_JSON_TYPE));
        ensureFAILResponse(response);
        MTResponse mtresp = response.readEntity(MTResponse.class);
        assertEquals(ErrorConstants.CURRENCY_MISMATCH, mtresp.getErrorCode());
    }
	
	/**
     * Negative Scenario:
     * Test funds transfer when source account has low balance
     * return 500 Internal Server Error
     */
	@Test
	public void testTransferForLowBalance() {
		Transaction txn = new Transaction(0L, 2, 3, new BigDecimal(250.00), "EUR", "comments", "PENDING");
        Response response = target.path("account/transfer").request(MediaType.APPLICATION_JSON_TYPE)
        		.post(Entity.entity(txn, MediaType.APPLICATION_JSON_TYPE));
        ensureFAILResponse(response);
        MTResponse mtresp = response.readEntity(MTResponse.class);
        assertEquals(ErrorConstants.LOW_ACCT_BALANCE, mtresp.getErrorCode());
    }
	
	/**
     * Negative Scenario:
     * Test funds transfer when currency of source & dest account doesn't match with request currency
     * return 500 Internal Server Error
     */
	@Test
	public void testTransferForInvalidCurrency() {
		Transaction txn = new Transaction(0L, 1, 2, new BigDecimal(50.00), "EUR", "comments", "PENDING");
        Response response = target.path("account/transfer").request(MediaType.APPLICATION_JSON_TYPE)
        		.post(Entity.entity(txn, MediaType.APPLICATION_JSON_TYPE));
        ensureFAILResponse(response);
        MTResponse mtresp = response.readEntity(MTResponse.class);
        assertEquals(ErrorConstants.CURRENCY_MISMATCH, mtresp.getErrorCode());
    }
	
	/**
     * Positive Scenario:
     * Test funds transfer success
     * return 200 OK
     */
	@Test
	public void testTransferSuccess() {
		Transaction txn = new Transaction(0L, 1, 2, new BigDecimal(50.00), "USD", "comments", "PENDING");
        Response response = target.path("account/transfer").request(MediaType.APPLICATION_JSON_TYPE)
        		.post(Entity.entity(txn, MediaType.APPLICATION_JSON_TYPE));
        ensureOKResponse(response);
        Account updatedAcct = response.readEntity(Account.class);
        assertEquals(250, updatedAcct.getBalance().intValue());
    }
	
	/**
     * Positive Scenario:
     * Test retrieval of all transactions
     * return 200 OK
     */
	@Test
    public void testGetAllTxnSuccess() {
        Response response = target.path("account/transaction/list").request().get();
        ensureOKResponse(response);
        List<Transaction> content = response.readEntity(List.class);
        assertEquals(2, content.size());
    }
    
	/**
     * Positive Scenario:
     * Test retrieval of specific transaction
     * return 200 OK
     */
	@Test
    public void testGetTxnByIdSuccess() {
        Response response = target.path("account/transaction/list/1").request().get();
        ensureOKResponse(response);
        Transaction txn = response.readEntity(Transaction.class);
        assertEquals(1, txn.getSourceAccountId());
        assertEquals(2, txn.getDestinationAccountId());
        assertEquals(100, txn.getAmount().intValue());
    }
    
	/**
     * Negative Scenario:
     * Test retrieval of a transaction which is not present in H2 DB
     * return 500 Internal Server Error
     */
	@Test
    public void testGetTxnByInvalidId() {
        Response response = target.path("account/transaction/list/4").request().get();
        ensureFAILResponse(response);
        MTResponse mtresp = response.readEntity(MTResponse.class);
        assertEquals(ErrorConstants.INVALID_TXN_ID, mtresp.getErrorCode());
    }
	
	
	private void ensureOKResponse(Response response) {
		assertEquals("Http Response should be 200: ", Status.OK.getStatusCode(), response.getStatus());
        assertEquals("Http Content-Type should be: ", MediaType.APPLICATION_JSON, response.getHeaderString(HttpHeaders.CONTENT_TYPE));
	}
	private void ensureFAILResponse(Response response) {
		assertEquals("Http Response should be 500: ", Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());
        assertEquals("Http Content-Type should be: ", MediaType.APPLICATION_JSON, response.getHeaderString(HttpHeaders.CONTENT_TYPE));
	}
}
